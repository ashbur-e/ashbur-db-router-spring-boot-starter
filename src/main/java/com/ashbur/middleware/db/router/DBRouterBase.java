package com.ashbur.middleware.db.router;

/**
 * @author : Eumenides
 * @Description : 数据源基础配置
 * @date : 2022/8/20
 * @Copyright ： 公众号：菜鸟的大厂梦
 */
public class DBRouterBase {

    private String tbIdx;

    public String getTbIdx() {
        return DBContextHolder.getTBKey();
    }

}
