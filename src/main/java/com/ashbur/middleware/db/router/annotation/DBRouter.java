package com.ashbur.middleware.db.router.annotation;

import java.lang.annotation.*;

/**
 * @author : Eumenides
 * @Description : 路由注解
 * @date : 2022/8/20
 * @Copyright ： 公众号：菜鸟的大厂梦
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface DBRouter {

    /** 分库分表字段 */
    String key() default "";

}
