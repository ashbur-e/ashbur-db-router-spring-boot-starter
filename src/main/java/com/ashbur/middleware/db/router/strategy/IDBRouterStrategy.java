package com.ashbur.middleware.db.router.strategy;

/**
 * @author : Eumenides
 * @Description : 路由策略
 * @date : 2022/8/20
 * @Copyright ： 公众号：菜鸟的大厂梦
 */
public interface IDBRouterStrategy {

    /**
     * 路由计算
     *
     * @param dbKeyAttr 路由字段
     */
    void doRouter(String dbKeyAttr);


    /**
     * 清除路由
     */
    void clear();

}
