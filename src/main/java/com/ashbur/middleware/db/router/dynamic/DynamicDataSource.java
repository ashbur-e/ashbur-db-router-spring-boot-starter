package com.ashbur.middleware.db.router.dynamic;

import com.ashbur.middleware.db.router.DBContextHolder;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author : Eumenides
 * @Description :
 * @date : 2022/8/20
 * @Copyright ： 公众号：菜鸟的大厂梦
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        return "db" + DBContextHolder.getDBKey();
    }
}
